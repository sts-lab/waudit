#!/usr/bin/env bash

if [ -x /sbin/auditctl ];
then
    AUDITCTL=/sbin/auditctl
else
    echo "Install auditctl. Run bootstrap.sh script"
    exit 0
fi
# systell calls to be audited.
#

# common calls
common="clone close creat dup dup2 dup3 execve
    exit exit_group fork open openat
    rename renameat
    read write
    unlink unlinkat vfork accept accept4 connect bind"

scall32=""

# 64 bit calls
scall64=""
#scall64_bind="bind"

build_syscall_args() {
    echo "in build syscall args"
    ARGS="-a exit,always "

    if [ $1 = "64" ]; then
        echo "arch: 64 bit"
        ARGS=$ARGS" -F arch=b64"
        ARGS_=$scall64
    elif [ $1 = "32" ]; then
        echo "arch: 32 bit"
        ARGS=$ARGS" -F arch=b32"
        ARGS_=$scall32
    else
        echo "Error invalid arch given: $1"
        exit -1
    fi

    for i in $common $ARGS_
        do
	    ARGS=$ARGS" -S $i"
        done

}

add() {
  if [ "$(uname -m | grep '64')" ]; then
      arch="64"
  else
      arch="32"
  fi

  if [ $arch = '64' ]; then
      build_syscall_args 64
      #echo $AUDITCTL $ARGS
      $AUDITCTL $ARGS
  fi

  build_syscall_args 32
  #echo $AUDITCTL $ARGS
  $AUDITCTL $ARGS

}

remove() {
    $AUDITCTL -D
}

enable() {
    $AUDITCTL -e1 -b 102400
}

disable() {
    $AUDITCTL -e0 -b 320
}

status() {
    $AUDITCTL -s
    echo "-----------------------------"
    $AUDITCTL -l
}


###main()###
case "$1" in
    add)
        remove
        add
        ;;
    remove)
        remove
        ;;
    enable)
        enable
        ;;
    disable)
        disable
        ;;
    status)
        status
        ;;
    *)
        echo "Usage: $0 add|remove|enable|disable"
        echo ""
        echo "add: add audit rules"
        echo "remove: auditctl -D"
        echo "enable: auditctl -e1 -b 1024000"
        echo "disable: auditctl -e0 -b 320"
        echo "status: auditctl -s -l"
esac
exit 0
