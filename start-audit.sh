#!/bin/bash
if [ $# -eq 0 ]; then
    echo "No arguments provided. Need argument for wlog. 1 means start app logging and 0 means no app logging"
    exit 1
fi

sudo service auditd stop
sudo rm /var/log/audit/audit.log*
sudo service auditd start
echo "ADDING KERNEL MODULES ===="
sudo bash addmods.sh "$1"

sudo bash audit-rules.sh add
sudo bash audit-rules.sh enable
echo "==== Status ===== "
sudo bash audit-rules.sh status
