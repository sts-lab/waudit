/*
   --------------------------------------------------------------------------------
   SPADE - Support for Provenance Auditing in Distributed Environments.
   Copyright (C) 2015 SRI International

   This program is free software: you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.
   --------------------------------------------------------------------------------
   */
#include <linux/unistd.h>  // __NR_<system-call-name>
#include <linux/file.h>
#include <linux/net.h>
#include <linux/audit.h>
#include <asm/paravirt.h> // write_cr0
#include <linux/uaccess.h>  // copy_from_user
#include <linux/kallsyms.h>
#include <linux/version.h>
#include <linux/fs.h>

#include <linux/slab.h>
#include <linux/dcache.h>
#include <linux/module.h>


#include "globals.h"

#define UENTRY		0xffffff9c // -100
#define UENTRY_ID	0xffffff9a // -102
#define UEXIT		0xffffff9b // -101
#define MREAD1		0xffffff38 // -200
#define MREAD2		0xffffff37 // -201
#define MWRITE1 	0xfffffed4 // -300
#define MWRITE2 	0xfffffed3 // -301
#define UDEP		0xfffffe70 // -400

#define BACKDOOR_KEY	0x00beefed

/*
 * 'stop' variable used to start and stop ONLY logging of system calls to audit log.
 * Don't need to synchronize 'stop' variable modification because it can only be set by a kernel module and only one
 * kernel module is updating it at the moment. Also, only one instance of a kernel module can be added at a time
 * hence ensuring no concurrent updates.
 */
static volatile int stop = 1;
static volatile int usingKey = 0;

static unsigned long syscall_table_address = 0;


// The return type for all system calls used is 'long' below even though some return 'int' according to API.
// Using 'long' because linux 64-bit kernel code uses a 'long' return value for all system calls.
// Error example: If 'int' used below for 'connect' system according to API then when negative value returned it gets
// casted to a shorter datatype (i.e. 'int') and incorrect return value is gotten. Hence the application using the
// 'connect' syscall fails.
/* asmlinkage long (*original_bind)(int, const struct sockaddr*, uint32_t); */
/* asmlinkage long (*original_connect)(int, const struct sockaddr*, uint32_t); */
asmlinkage long (*original_write)(int fd, const void *buf, size_t count);
/* asmlinkage long (*original_accept)(int, struct sockaddr*, uint32_t*); */
/* asmlinkage long (*original_accept4)(int, struct sockaddr*, uint32_t*, int); */


static int is_sockaddr_size_valid(uint32_t size);
static void to_hex(unsigned char *dst, uint32_t dst_len, unsigned char *src, uint32_t src_len);
static void sockaddr_to_hex(unsigned char* dst, int dst_len, unsigned char* addr, uint32_t addr_size);
static int copy_sockaddr_from_user(struct sockaddr_storage *dst, const struct sockaddr __user *src, uint32_t src_size);
static int copy_uint32_t_from_user(uint32_t *dst, const uint32_t __user *src);
static int exists_in_array(int, int[], int);
static int log_syscall(int, int, int, int);
static void copy_array(int* dst, int* src, int len);
static int netio_logging_start(char* caller_build_hash, int net_io_flag, int wlog_flag, int syscall_success_flag,
        int pids_ignore_length, int pids_ignore_list[],
        int ppids_ignore_length, int ppids_ignore_list[],
        int uids_len, int uids[], int ignore_uids, char* passed_key,
        int harden_tgids_length, int harden_tgids_list[]); // 1 success, 0 failure
static void netio_logging_stop(char* caller_build_hash);


static void copy_array(int* dst, int* src, int len){
    int a = 0;
    for(; a < len; a++){
        dst[a] = src[a];
    }
}

static int exists_in_array(int id, int arr[], int arrlen){
    int i;
    for(i = 0; i<arrlen; i++){
        if(arr[i] == id){
            return 1;
        }
    }
    return -1;
}

static int log_syscall(int pid, int ppid, int uid, int success){
    if(syscall_success == -1){ // log all
        // log it if other filters matched
    }else if(syscall_success == 0){ // only log failed ones
        if(success == 1){ // successful so don't log
            return -1;
        }
    }else if(syscall_success == 1){ // only log successful ones
        if(success == 0){
            return -1; //failed so don't log
        }
    }

    if(ignore_uids == 1){
        if(exists_in_array(uid, uids, uids_len) > 0){
            return -1;
        }
    }else{ // ignore_uids = 0 i.e. capture the uid in the list
        if(exists_in_array(uid, uids, uids_len) <= 0){
            return -1;
        }
    }

    if(exists_in_array(pid, pids_ignore, pids_ignore_len) > 0){
        return -1;
    }

    if(exists_in_array(ppid, ppids_ignore, ppids_ignore_len) > 0){
        return -1;
    }
    return 1;
}

static int copy_uint32_t_from_user(uint32_t *dst, const uint32_t __user *src){
    return copy_from_user(dst, src, sizeof(uint32_t));
}

// 0 = bad, 1 = good
static int is_sockaddr_size_valid(uint32_t size){
    if(size > 0 && size <= sizeof(struct sockaddr_storage)){
        return 1;
    }else{
        return 0;
    }
}

// Follows copy_from_user return value rules. 0 is good, other is bad.
static int copy_sockaddr_from_user(struct sockaddr_storage *dst, const struct sockaddr __user *src, uint32_t src_size){
    if(is_sockaddr_size_valid(src_size) == 0){
        return -1; // error
    }
    return copy_from_user(dst, src, src_size);
}


// dst should be large enough and caller should ensure that.
static void to_hex(unsigned char *dst, uint32_t dst_len, unsigned char *src, uint32_t src_len){
    int i;
    memset(dst, 0, dst_len);
    for (i = 0; i < src_len; i++){
        *dst++ = hex_asc_upper[((src[i]) & 0xf0) >> 4];
        *dst++ = hex_asc_upper[((src[i]) & 0x0f)];
    }
    *dst = 0; // NULL char
}

// dst should be large enough and caller should ensure that.
static void sockaddr_to_hex(unsigned char* dst, int dst_len, unsigned char* addr, uint32_t addr_size){
    if(addr != NULL && is_sockaddr_size_valid(addr_size) == 1){
        to_hex(dst, dst_len, addr, addr_size);
    }else{
        // Make dst null string
        *dst = 0;
    }
}

static void log_to_audit(int syscallNumber, int fd, struct sockaddr_storage* addr, uint32_t addr_size,
        long exit, int success){
    struct task_struct* current_task = current;
    if(log_syscall((int)(current_task->pid), (int)(current_task->real_parent->pid),
                (int)(current_task->real_cred->uid.val), success) > 0){

        struct socket *fd_sock;
        int fd_sock_type = -1;
        int fd_sock_family = -1;
        struct sockaddr_storage fd_addr;
        int fd_addr_size = 0;
        int err = 0;

        int max_hex_sockaddr_size = (_K_SS_MAXSIZE * 2) + 1; // +1 for NULL char
        unsigned char hex_fd_addr[max_hex_sockaddr_size];
        unsigned char hex_addr[max_hex_sockaddr_size];

        char* task_command = current_task->comm;
        int task_command_len = strlen(task_command);
        int hex_task_command_len = (task_command_len * 2) + 1; // +1 for NULL
        unsigned char hex_task_command[hex_task_command_len];
        // get command
        to_hex(&hex_task_command[0], hex_task_command_len, (unsigned char *)task_command, task_command_len);
        // get addr passed in
        sockaddr_to_hex(&hex_addr[0], max_hex_sockaddr_size, (unsigned char *)addr, addr_size);

        // This call places a lock on the fd which prevents it from being released.
        // Releasing the lock using sockfd_put call.
        fd_sock = sockfd_lookup(fd, &err);

        if(fd_sock != NULL){
            fd_sock_family = fd_sock->ops->family;
            fd_sock_type = fd_sock->type;
            if((fd_sock_family != AF_UNIX && fd_sock_family != AF_LOCAL && fd_sock_family != PF_UNIX
                        && fd_sock_family != PF_LOCAL && fd_sock_family != AF_INET && fd_sock_family != AF_INET6
                        && fd_sock_family != PF_INET && fd_sock_family != PF_INET6) && // only unix, and inet
                    (fd_sock_type != SOCK_STREAM && fd_sock_type != SOCK_DGRAM)){ // only stream, and dgram
                return;
            }else{
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4, 17, 0)
                fd_addr_size = fd_sock->ops->getname(fd_sock, (struct sockaddr *)&fd_addr, 0);
                if(fd_addr_size <= 0){
                    err = -1;
                }else{
                    err = 0;
                }
#else
                err = fd_sock->ops->getname(fd_sock, (struct sockaddr *)&fd_addr, &fd_addr_size, 0);
#endif
                if(err == 0){
                    sockaddr_to_hex(&hex_fd_addr[0], max_hex_sockaddr_size, (unsigned char*)&fd_addr, fd_addr_size);
                }
            }
            // Must free the reference to the fd after use otherwise lock won't be released.
            sockfd_put(fd_sock);
        }

        // TODO other info needs to be logged?
        audit_log(NULL, GFP_KERNEL, AUDIT_USER,
                "netio_intercepted=\"syscall=%d exit=%ld success=%d fd=%d pid=%d ppid=%d uid=%u euid=%u suid=%u fsuid=%u \
                gid=%u egid=%u sgid=%u fsgid=%u comm=%s sock_type=%d local_saddr=%s remote_saddr=%s remote_saddr_size=%d\"",
                syscallNumber, exit, success, fd, current_task->pid, current_task->real_parent->pid,
                current_task->real_cred->uid.val, current_task->real_cred->euid.val, current_task->real_cred->suid.val,
                current_task->real_cred->fsuid.val, current_task->real_cred->gid.val, current_task->real_cred->egid.val,
                current_task->real_cred->sgid.val, current_task->real_cred->fsgid.val,
                hex_task_command, fd_sock_type, hex_fd_addr, hex_addr, addr_size);
    }
}
uint8_t strContains(char* string, char* toFind)
{
    uint8_t slen = strlen(string);
    uint8_t tFlen = strlen(toFind);
    uint8_t found = 0;

    if( slen >= tFlen )
    {
        uint8_t s;
        uint8_t t;
        for(s=0, t=0; s<slen; s++)
        {
            do{

                if( string[s] == toFind[t] )
                {
                    if( ++found == tFlen ) return 1;
                    s++;
                    t++;
                }
                else { s -= found; found=0; t=0; }

            }while(found);
        }
        return 0;
    }
    else return -1;
}

static inline int should_log_write(char *fp)
{
    return (fp && 
            ((strstr(fp,"/var/log/nginx/") != NULL) || 
             (strstr(fp,"/var/log/apache2/") != NULL) ||
             (strstr(fp,"/var/log/redis/redis-server.log") != NULL) || 
             (strstr(fp,"/var/log/proftpd/") != NULL) ||
             (strstr(fp,"/tmp/proftpd.log") != NULL) ||
             (strstr(fp,"/var/log/haproxy/") != NULL) || 
             (strstr(fp,"/var/log/memcached/") != NULL) || 
             (strstr(fp,"/var/log/squid/") != NULL) || 
             (strstr(fp,"/home/mohammad/Playground/squid-SQUID_4_6/install/var/logs") != NULL) ||
             (strstr(fp,"/var/log/sshd.log") != NULL) || 
             (strstr(fp,"/var/log/transmission.log") != NULL) || 
             (strstr(fp,"/tmp/transmission.log") != NULL) || 
             (strstr(fp, "/home/mohammad/Playground/nginx-1.17.8/install/logs/") != NULL) ||
             (strstr(fp, "/home/mohammad/Playground/proftpd-1.3.6c/install/var/log/proftpd/") != NULL) ||
             (strstr(fp, "tmp/memcached.log") != NULL) ||
             (strstr(fp, "/tmp/simple") != NULL) ||
             (strstr(fp, "/tmp/thttpd.log") != NULL) ||
             (strstr(fp, "/tmp/wget.log") != NULL))
           );
}

asmlinkage long new_write(int fd, const void *buf, size_t count){
    // flag to trun on and off application logging
    if(wlog == 0){
        long retval = original_write(fd, buf, count);
        return retval;
    }
    struct task_struct* current_task = current;
    int syscallNumber = __NR_write;
    struct fd f = fdget(fd);
    char path_buff[PATH_MAX];
    char* fp;
    if (!f.file){
        long retval = original_write(fd, buf, count);
        fdput(f);
        return retval;
    }
    fp = dentry_path_raw(f.file->f_path.dentry,path_buff,sizeof(path_buff));
    //  if (fp && ((strstr(fp,"/var/log/nginx/") != NULL) || (strstr(fp,"/var/log/apache2/") != NULL) ||
    //	     (strstr(fp,"/var/log/redis/redis-server.log") != NULL) || (strstr(fp,"/var/log/proftpd/") != NULL) ||
    //	     (strstr(fp,"/var/log/haproxy/") != NULL) || (strstr(fp,"/var/log/memcached/") != NULL)
    //	     || (strstr(fp,"/var/log/squid/") != NULL) || (strstr(fp,"/var/log/sshd.log") != NULL)
    //	     || (strstr(fp,"/var/log/transmission.log") != NULL) || (strstr(fp, "/home/mohammad/Playground/nginx-1.17.8/install/logs/") != NULL) ) ){
    if (should_log_write(fp)) {
        int i = 0, j = 0;
        char *temp =  (char *) kmalloc(count, GFP_USER);
        char *hex =  (char *) kmalloc(((2*count)+1), GFP_USER);
        long copied = strncpy_from_user(temp, buf, count);
        if (copied<0){
            //temp = "failed\n\0";
        }else{
            for (i=0, j=0;i < (count) ; ++i,j += 2)
                sprintf((char*) hex + j, "%02X", temp[i]);
            hex[2*count] = '\0';
            audit_log(NULL, GFP_KERNEL, AUDIT_USER,
                    "applog=\"syscall=%d fd=%d path=%s pid=%d ppid=%d uid=%u data=%s\"",
                    syscallNumber, fd, fp,current_task->pid, current_task->real_parent->pid,
                    current_task->real_cred->uid.val, hex);
            /* printk(KERN_EMERG "wajih: %s", hex); */
        }
        kfree(temp);
        kfree(hex);
    }
    long retval = original_write(fd, buf, count);
    fdput(f);
    return retval;
}



/* asmlinkage long new_bind(int fd, const struct sockaddr __user *addr, uint32_t addr_size){ */
/*   long retval = original_bind(fd, addr, addr_size); */
/*   if(stop == 0){ */
/*     struct sockaddr_storage k_addr; */
/*     int syscallNumber = __NR_bind; */
/*     int success = retval == 0 ? 1 : 0; */

/*     // Order of conditions matters! */
/*     if(addr != NULL && is_sockaddr_size_valid(addr_size) == 1 */
/*        && copy_sockaddr_from_user(&k_addr, addr, addr_size) == 0){ */
/*       log_to_audit(syscallNumber, fd, &k_addr, addr_size, retval, success); */
/*     }else{ */
/*       log_to_audit(syscallNumber, fd, NULL, 0, retval, success); */
/*     } */
/*   } */
/*   return retval; */
/* } */

/* asmlinkage long new_connect(int fd, const struct sockaddr __user *addr, uint32_t addr_size){ */
/* 	long retval = original_connect(fd, addr, addr_size); */
/* 	if(stop == 0){ */
/* 		struct sockaddr_storage k_addr; */
/* 		int syscallNumber = __NR_connect; */
/* 		int success = (retval >= 0 || retval == -EINPROGRESS) ? 1 : 0; */

/* 		// Order of conditions matters! */
/* 		if(addr != NULL && is_sockaddr_size_valid(addr_size) == 1 */
/* 				&& copy_sockaddr_from_user(&k_addr, addr, addr_size) == 0){ */
/* 			log_to_audit(syscallNumber, fd, &k_addr, addr_size, retval, success); */
/* 		}else{ */
/* 			log_to_audit(syscallNumber, fd, NULL, 0, retval, success); */
/* 		} */
/* 	} */
/* 	return retval; */
/* } */

/* asmlinkage long new_accept(int fd, struct sockaddr __user *addr, uint32_t __user *addr_size){ */
/* 	long retval = original_accept(fd, addr, addr_size); */
/* 	if(stop == 0){ */
/* 		uint32_t k_addr_size; */
/* 		struct sockaddr_storage k_addr; */
/* 		int syscallNumber = __NR_accept; */
/* 		int success = retval >= 0 ? 1 : 0; */

/* 		// Order of conditions matters! */
/* 		if(addr != NULL && addr_size != NULL */
/* 				&& copy_uint32_t_from_user(&k_addr_size, addr_size) == 0 */
/* 				&& is_sockaddr_size_valid(k_addr_size) == 1 */
/* 				&& copy_sockaddr_from_user(&k_addr, addr, k_addr_size) == 0){ */
/* 			log_to_audit(syscallNumber, fd, &k_addr, k_addr_size, retval, success); */
/* 		}else{ */
/* 			log_to_audit(syscallNumber, fd, NULL, 0, retval, success); */
/* 		} */
/* 	} */
/* 	return retval; */
/* } */


/* asmlinkage long new_accept4(int fd, struct sockaddr __user *addr, uint32_t __user *addr_size, int flags){ */
/* 	long retval = original_accept4(fd, addr, addr_size, flags); */
/* 	if(stop == 0){ */
/* 		uint32_t k_addr_size; */
/* 		struct sockaddr_storage k_addr; */
/* 		int syscallNumber = __NR_accept4; */
/* 		int success = retval >= 0 ? 1 : 0; */

/* 		// Order of conditions matters! */
/* 		if(addr != NULL && addr_size != NULL */
/* 				&& copy_uint32_t_from_user(&k_addr_size, addr_size) == 0 */
/* 				&& is_sockaddr_size_valid(k_addr_size) == 1 */
/* 				&& copy_sockaddr_from_user(&k_addr, addr, k_addr_size) == 0){ */
/* 			log_to_audit(syscallNumber, fd, &k_addr, k_addr_size, retval, success); */
/* 		}else{ */
/* 			log_to_audit(syscallNumber, fd, NULL, 0, retval, success); */
/* 		} */
/* 	} */
/* 	return retval; */
/* } */

static int __init onload(void) {
    int success = -1;
    syscall_table_address = kallsyms_lookup_name("sys_call_table");
    //printk(KERN_EMERG "sys_call_table address = %lx\n", syscall_table_address);
    if(syscall_table_address != 0){
        unsigned long* syscall_table = (unsigned long*)syscall_table_address;
        write_cr0 (read_cr0 () & (~ 0x10000));
        /* original_bind = (void *)syscall_table[__NR_bind]; */
        /* syscall_table[__NR_bind] = (unsigned long)&new_bind; */

        /* original_connect = (void *)syscall_table[__NR_connect]; */
        /* syscall_table[__NR_connect] = (unsigned long)&new_connect; */

        original_write = (void *)syscall_table[__NR_write];
        syscall_table[__NR_write] = (unsigned long)&new_write;

        /* original_accept = (void *)syscall_table[__NR_accept]; */
        /* syscall_table[__NR_accept] = (unsigned long)&new_accept; */
        /* original_accept4 = (void *)syscall_table[__NR_accept4]; */
        /* syscall_table[__NR_accept4] = (unsigned long)&new_accept4; */


        write_cr0 (read_cr0 () | 0x10000);
        printk(KERN_EMERG "[%s] system call table hooked\n", MAIN_MODULE_NAME);
        success = 0;
    }else{
        printk(KERN_EMERG "[%s] system call table address not initialized\n", MAIN_MODULE_NAME);
        success = -1;
    }
    /*
     * A non 0 return means init_module failed; module can't be loaded.
     */
    return success;
}

static void __exit onunload(void) {
    if (syscall_table_address != 0) {
        unsigned long* syscall_table = (unsigned long*)syscall_table_address;
        write_cr0 (read_cr0 () & (~ 0x10000));
        /* syscall_table[__NR_bind] = (unsigned long)original_bind; */
        /* syscall_table[__NR_connect] = (unsigned long)original_connect; */
        syscall_table[__NR_write] = (unsigned long)original_write;
        /* syscall_table[__NR_accept] = (unsigned long)original_accept; */
        /* syscall_table[__NR_accept4] = (unsigned long)original_accept4; */

        write_cr0 (read_cr0 () | 0x10000);
        printk(KERN_EMERG "[%s] system call table unhooked\n", MAIN_MODULE_NAME);
    } else {
        printk(KERN_EMERG "[%s] system call table address not initialized\n", MAIN_MODULE_NAME);
    }
}

static int netio_logging_start(char* caller_build_hash, int net_io_flag, int wlog_flag, int syscall_success_flag,
        int pids_ignore_length, int pids_ignore_list[],
        int ppids_ignore_length, int ppids_ignore_list[],
        int uids_length, int uids_list[], int ignore_uids_flag, char* passed_key,
        int harden_tgids_length, int harden_tgids_list[]){
    if(str_equal(caller_build_hash, BUILD_HASH) == 1){
        net_io = net_io_flag;
        wlog= wlog_flag;
        syscall_success = syscall_success_flag;
        ignore_uids = ignore_uids_flag;
        pids_ignore_len = pids_ignore_length;
        ppids_ignore_len = ppids_ignore_length;
        uids_len = uids_length;
        key = passed_key;
        harden_tgids_len = harden_tgids_length;

        copy_array(&pids_ignore[0], &pids_ignore_list[0], pids_ignore_len);
        copy_array(&ppids_ignore[0], &ppids_ignore_list[0], ppids_ignore_len);
        copy_array(&uids[0], &uids_list[0], uids_len);
        copy_array(&harden_tgids[0], &harden_tgids_list[0], harden_tgids_len);

        if(str_equal(NO_KEY, key) != 1){
            usingKey = 1;
        }

        print_args(MAIN_MODULE_NAME);
        printk(KERN_EMERG "[%s] Logging started!\n", MAIN_MODULE_NAME);

        stop = 0;

        return 1;
    }else{
        printk(KERN_EMERG "[%s] SEVERE Build mismatch. Rebuild, remove, and add ALL modules. Logging NOT started!\n", MAIN_MODULE_NAME);
        return 0;
    }
}

static void netio_logging_stop(char* caller_build_hash){
    if(str_equal(caller_build_hash, BUILD_HASH) == 1){
        printk(KERN_EMERG "[%s] Logging stopped!\n", MAIN_MODULE_NAME);
        stop = 1;
        usingKey = 0;
    }else{
        printk(KERN_EMERG "[%s] SEVERE Build mismatch. Rebuild, remove, and add ALL modules. Logging NOT stopped!\n", MAIN_MODULE_NAME);
    }
}

EXPORT_SYMBOL(netio_logging_start);
EXPORT_SYMBOL(netio_logging_stop);

module_init(onload);
module_exit(onunload);
