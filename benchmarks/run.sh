#!/bin/bash

bash ~/projects/waudit/benchmarks/stop-all.sh
VAR=`bash ~/projects/waudit/benchmarks/apache-exp.sh | tail -1`
echo "Httpd $VAR"
bash ~/projects/waudit/benchmarks/stop-all.sh
VAR=`bash ~/projects/waudit/benchmarks/nginx-exp.sh | tail -1`
echo "NGINX $VAR"
bash ~/projects/waudit/benchmarks/stop-all.sh
VAR=`bash ~/projects/waudit/benchmarks/ftp-exp.sh | tail -1`
echo "Proftpd $VAR"
bash ~/projects/waudit/benchmarks/stop-all.sh
VAR=`bash ~/projects/waudit/benchmarks/memcache-exp.sh | tail -1`
echo "Memcached $VAR"
bash ~/projects/waudit/benchmarks/stop-all.sh
VAR=`bash ~/projects/waudit/benchmarks/openssh-exp.sh | tail -1`
echo "OpenSSH $VAR"
bash ~/projects/waudit/benchmarks/stop-all.sh
VAR=`bash ~/projects/waudit/benchmarks/redis-exp.sh | tail -1`
echo "Redis $VAR"
bash ~/projects/waudit/benchmarks/stop-all.sh
VAR=`bash ~/projects/waudit/benchmarks/squid-exp.sh | tail -1`
echo "Squid $VAR"
bash ~/projects/waudit/benchmarks/stop-all.sh
VAR=`bash ~/projects/waudit/benchmarks/transmission-exp.sh | tail -1`
echo "Transmission $VAR"
bash ~/projects/waudit/benchmarks/stop-all.sh
