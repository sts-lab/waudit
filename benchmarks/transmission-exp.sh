sudo bash ~/projects/waudit/benchmarks/stop-all.sh
sudo service transmission-daemon start
sudo service transmission-daemon reload
sudo bash ~/projects/waudit/start-audit.sh 0
sleep 2
starttime=`date +%s.%N`
sudo service transmission-daemon reload
transmission-remote -n 'transmission:transmission' -r
for i in $(seq 1 10)
do
    transmission-remote -n 'transmission:transmission' -r
    transmission-remote -n 'transmission:transmission' -a ./test.torrent
    transmission-remote -n 'transmission:transmission' -st
    transmission-remote -n 'transmission:transmission' -r

done
endtime=`date +%s.%N`
sudo bash ~/projects/waudit/stop-audit.sh
echo "Totaltime:"
echo "$endtime $starttime" | awk '{print $1-$2}'
old=`echo "$endtime $starttime" | awk '{print $1-$2}'`
count=`cat /var/log/audit/audit.log* | grep applog  | wc -l`
echo "---================COUNT $count"
sleep 2

sudo service transmission-daemon restart
transmission-remote -n 'transmission:transmission' -r
sudo bash ~/projects/waudit/start-audit.sh 1

sleep 2
starttime=`date +%s.%N`
for i in $(seq 1 10)
do
    transmission-remote -n 'transmission:transmission' -r
    transmission-remote -n 'transmission:transmission' -a ./test.torrent
    transmission-remote -n 'transmission:transmission' -st
    transmission-remote -n 'transmission:transmission' -r
done
endtime=`date +%s.%N`
sudo service transmission-daemon stop
sudo bash ~/projects/waudit/stop-audit.sh
count=`cat /var/log/audit/audit.log* | grep applog  | wc -l`
echo "---================COUNT $count"

echo "Totaltime:"
echo "$endtime $starttime" | awk '{print $1-$2}'
new=`echo "$endtime $starttime" | awk '{print $1-$2}' `
diff=`echo "$new-$old" | bc -l`
echo "$diff/$old * 100.0" | bc -l
