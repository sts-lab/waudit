#!/bin/bash
sudo service memcached stop
sudo bash ~/projects/waudit/benchmarks/stop-all.sh
ps -fA | grep -E 'memcached'| awk '{print $2}'| while read line; do sudo kill -09 "$line"; done

sleep 2
sudo sh -c  '/usr/bin/memcached -vv -m 64 -p 11211 -u root -l 127.0.0.1 -P /var/run/memcached/memcached.pid 2>> /var/log/memcached/memcached.log &'

sudo bash ~/projects/waudit/start-audit.sh 0
sleep 2
starttime=`date +%s.%N`
echo "starting loop"
for i in $(seq 1 20)
do
    ./mc-benchmark/mc-benchmark -h 127.0.0.1 -n 1000 -c 10 > /dev/null
done
endtime=`date +%s.%N`
sudo service memcached stop
ps -fA | grep -E 'memcached'| awk '{print $2}'| while read line; do sudo kill -09 "$line"; done


sudo bash ~/projects/waudit/stop-audit.sh
echo "Totaltime:"
echo "$endtime $starttime" | awk '{print $1-$2}'
old=`echo "$endtime $starttime" | awk '{print $1-$2}'`
count=`cat /var/log/audit/audit.log* | grep applog  | wc -l`
echo "\\n---================COUNT $count ==========================================="
sleep 2


sudo sh -c  '/usr/bin/memcached -vv -m 64 -p 11211 -u root -l 127.0.0.1 -P /var/run/memcached/memcached.pid 2>> /var/log/memcached/memcached.log &'
sudo bash ~/projects/waudit/start-audit.sh 1
sleep 2
starttime=`date +%s.%N`
echo "starting loop"
for i in $(seq 1 20)
do
    ./mc-benchmark/mc-benchmark -h 127.0.0.1 -n 1000 -c 10 > /dev/null
done
endtime=`date +%s.%N`

sudo service memcached stop
ps -fA | grep -E 'memcached'| awk '{print $2}'| while read line; do sudo kill -09 "$line"; done

sudo bash ~/projects/waudit/stop-audit.sh
count=`cat /var/log/audit/audit.log* | grep applog  | wc -l`
echo "\\n---================COUNT $count ==========================================="

echo "Totaltime:"
echo "$endtime $starttime" | awk '{print $1-$2}'
new=`echo "$endtime $starttime" | awk '{print $1-$2}' `


diff=`echo "$new-$old" | bc -l`
echo "$diff/$old * 100.0" | bc -l
