#!/bin/bash

sudo bash ~/projects/waudit/benchmarks/stop-all.sh
sudo systemctl restart redis.service
sudo bash ~/projects/waudit/start-audit.sh 1
sleep 2
for i in $(seq 1 3)
do
    redis-benchmark -t lpush -n 1 -q -c 1

done
sudo systemctl stop redis.service
sudo bash ~/projects/waudit/stop-audit.sh
count=`cat /var/log/audit/audit.log* | grep applog  | wc -l`
echo "---================COUNT $count"
