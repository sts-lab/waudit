#!/bin/bash

BINDIR="/home/mohammad/Playground/transmission-2.94/install/bin"

set -x
$BINDIR/transmission-remote -n 'transmission:transmission' -r
$BINDIR/transmission-remote -n 'transmission:transmission' -a ./test.torrent
$BINDIR/transmission-remote -n 'transmission:transmission' -st
$BINDIR/transmission-remote -n 'transmission:transmission' -r
