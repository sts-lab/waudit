#!/bin/bash

sudo bash ~/projects/waudit/benchmarks/stop-all.sh

sudo service proftpd restart
sudo bash ~/projects/waudit/start-audit.sh 0
sleep 2
starttime=`date +%s.%N`
for i in $(seq 1 2)
do
    ftpbench -h 127.0.0.1 -u myproftpduser -p wlog --concurrent=5 login
    ftpbench -h 127.0.0.1 -u myproftpduser -p wlog --size=1 --files=5 --concurrent=5  download /tmp/
done
endtime=`date +%s.%N`
sudo service proftpd stop

sudo bash ~/projects/waudit/stop-audit.sh
echo "Totaltime:"
echo "$endtime $starttime" | awk '{print $1-$2}'
old=`echo "$endtime $starttime" | awk '{print $1-$2}'`
count=`cat /var/log/audit/audit.log* | grep applog  | wc -l`
echo "---================COUNT $count"
sleep 2


sudo service proftpd restart
sudo bash ~/projects/waudit/start-audit.sh 1
sleep 2
starttime=`date +%s.%N`
for i in $(seq 1 2)
do
    ftpbench -h 127.0.0.1 -u myproftpduser -p wlog --concurrent=5 login
    ftpbench -h 127.0.0.1 -u myproftpduser -p wlog --size=1 --files=5 --concurrent=5  download /tmp/
done
endtime=`date +%s.%N`
sudo service proftpd stop
sudo bash ~/projects/waudit/stop-audit.sh
count=`cat /var/log/audit/audit.log* | grep applog  | wc -l`
echo "---================COUNT $count"

echo "Totaltime:"
echo "$endtime $starttime" | awk '{print $1-$2}'
new=`echo "$endtime $starttime" | awk '{print $1-$2}' `


diff=`echo "$new-$old" | bc -l`
echo "$diff/$old * 100.0" | bc -l
