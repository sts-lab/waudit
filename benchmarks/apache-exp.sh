#!/bin/bash

sudo bash ~/projects/waudit/benchmarks/stop-all.sh
sudo systemctl restart apache2.service
sudo bash ~/projects/waudit/start-audit.sh 0
sleep 2
starttime=`date +%s.%N`
for i in $(seq 1 100)
do
    ab -n 500 -c 2 http://localhost:80/ &> output_old.txt
done
endtime=`date +%s.%N`
sudo systemctl stop apache2.service
sudo bash ~/projects/waudit/stop-audit.sh
echo "Totaltime:"
echo "$endtime $starttime" | awk '{print $1-$2}'
old=`echo "$endtime $starttime" | awk '{print $1-$2}'`
count=`cat /var/log/audit/audit.log* | grep applog  | wc -l`
echo "---================COUNT $count"
sleep 2
sudo systemctl restart apache2.service
sudo bash ~/projects/waudit/start-audit.sh 1
sleep 2
starttime=`date +%s.%N`
for i in $(seq 1 100)
do
    ab -n 500 -c 2 http://localhost:80/ &> output_new.txt
done
endtime=`date +%s.%N`
sudo systemctl stop apache2.service
sudo bash ~/projects/waudit/stop-audit.sh
count=`cat /var/log/audit/audit.log* | grep applog  | wc -l`
echo "---================COUNT $count"

echo "Totaltime:"
echo "$endtime $starttime" | awk '{print $1-$2}'
new=`echo "$endtime $starttime" | awk '{print $1-$2}' `
diff=`echo "$new-$old" | bc -l`
echo "$diff/$old * 100.0" | bc -l
