sudo bash ~/projects/waudit/benchmarks/stop-all.sh

sudo service sshd start
sudo service sshd reload
sudo service ssh start
sudo service rsyslog restart
sudo bash ~/projects/waudit/start-audit.sh 0
sleep 2
starttime=`date +%s.%N`
for i in $(seq 1 1500)
do
    sshpass -p 'wlog'  ssh myproftpduser@127.0.0.1 "pwd"
    sshpass -p 'wlog'  ssh myproftpduser@127.0.0.1 "ls /"
    wait
done
endtime=`date +%s.%N`
sudo service sshd stop

sudo bash ~/projects/waudit/stop-audit.sh
echo "Totaltime:"
echo "$endtime $starttime" | awk '{print $1-$2}'
old=`echo "$endtime $starttime" | awk '{print $1-$2}'`
count=`cat /var/log/audit/audit.log* | grep applog  | wc -l`
echo "---================COUNT $count"
sleep 2
sudo service rsyslog restart
sudo service sshd start
sudo service sshd reload
sudo service ssh start
sudo service rsyslog restart
sudo bash ~/projects/waudit/start-audit.sh 1
sleep 2
starttime=`date +%s.%N`
for i in $(seq 1 1500)
do
    sshpass -p 'wlog'  ssh myproftpduser@127.0.0.1 "pwd"
    sshpass -p 'wlog'  ssh myproftpduser@127.0.0.1 "ls /"
    wait
done
endtime=`date +%s.%N`
sudo service sshd stop
sudo bash ~/projects/waudit/stop-audit.sh
count=`cat /var/log/audit/audit.log* | grep applog  | wc -l`
echo "---================COUNT $count"

echo "Totaltime:"
echo "$endtime $starttime" | awk '{print $1-$2}'
new=`echo "$endtime $starttime" | awk '{print $1-$2}' `
diff=`echo "$new-$old" | bc -l`
echo "$diff/$old * 100.0" | bc -l
