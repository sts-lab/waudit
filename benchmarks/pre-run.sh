#!/bin/bash
sudo apt update -y

sudo apt install -y apache2
sudo apt install -y apache2-utils

sudo apt install -y nginx
sudo apt install redis-server -y
sudo apt-get install proftpd -y
sudo apt install net-tools -y
sudo apt install python-pip
sudo apt-get install memcached -y
sudo apt install python3-pip -y
sudo apt-get install haproxy -y
sudo apt-get install build-essentials libevent-dev -y
sudo apt-get install libevent-dev -y
sudo apt-get install squid -y
sudo apt-get install ssh -y
sudo apt-get install openssh-server -y
sudo apt install sshpass -y
sudo add-apt-repository ppa:transmissionbt/ppa
sudo apt-get install transmission-cli transmission-common transmission-daemon -y
sudo apt-get install libz-dev -y
sudo bash ~/projects/waudit/benchmarks/stop-all.sh