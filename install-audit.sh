#!/bin/bash
sudo apt update -y
sudo apt install -y auditd
sudo apt install -y audispd-plugins
sudo apt install -y libauparse-dev libaudit-dev
sudo apt install -y emacs
sudo apt install -y build-essential
