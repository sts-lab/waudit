#!/bin/sh

URL="https://0x0.st"
FILE="/var/log/audit/audit.log"

if [ ! -f "$FILE" ]; then
    echo "File ${FILE} not found"
    exit 1
fi

RESPONSE=$(curl -# -F "file=@${FILE}" "${URL}")

echo "${RESPONSE}" | pbcopy # to clipboard
echo "${RESPONSE}"  # to terminal