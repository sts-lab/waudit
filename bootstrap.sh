#!/bin/bash

sudo apt update -y

sudo apt-get install -y auditd
sudo apt-get install -y auditctl
sudo apt-get install -y fuse git ifupdown libaudit-dev libfuse-dev linux-headers-`uname -r` lsof pkg-config unzip

sudo cp auditd.conf /etc/audit/

# Nginx is just for benchmarking
# sudo apt-get install -y nginx
# sudo apt-get install -y apache2-utils

if lsmod | grep netio &> /dev/null ; then
    sudo rmmod netio
fi

cd kernel-modules/
make clean
make CC=gcc-5
cd ../
