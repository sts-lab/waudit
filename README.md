# Collecting Audit logs on Linux using Auditd and Custom Kernel Modules

### USAGE

For the first time please run the following script to install prerequisite softwares and correct configuration files. It is recommended that you turn on passwordless sudo access but it will work anyways.

```sh
sudo bash bootstrap.sh
```

Once the bootstrap process finishes, you can start collecting logs using following command:

```sh
sudo bash start-audit.sh [0/1]
```
This command will generate logs in `/var/log/audit/` directory. Pass either "0" or "1" to turn off and on application logging for wlog respectively.

To stop collecting logs run following command:

```sh
sudo bash stop-audit.sh
```

To collect application logs it intercepts the write call can check if it is in /var/log/ directory.
After that we check if we are writing to in one of the defined application logs such as nginx or apache.
Currently following applications are supported:

```c

if ((strstr(fp,"nginx") != NULL) || (strstr(fp,"apache") != NULL) || (strstr(fp,"haproxy") != NULL) || (strstr(fp,"proftpd") != NULL)
	|| (strstr(fp,"redis") != NULL)  || (strstr(fp,"squid") != NULL) || (strstr(fp,"postgresql") != NULL) || (strstr(fp,"openssh") != NULL)
	||  (strstr(fp,"postfix") != NULL) || (strstr(fp,"memcached") != NULL) ) {
```

You can change above line in netio.c file in this repo to add more applications or contact wajih.