set -e
MODULE="netio"
if lsmod | grep "$MODULE" &> /dev/null ; then
  echo "$MODULE is loaded!"
else
    echo "$MODULE is not loaded! So I am loading it"
    insmod ./kernel-modules/netio.ko
fi

echo "Flag for app logging $1"

insmod ./kernel-modules/netio_controller.ko wlog="$1" pids_ignore="`pidof auditd`,`pidof kauditd`" ppids_ignore="`pidof auditd`,`pidof kauditd`" syscall_success="1"

echo " ======= Done adding netio controller  ==========="