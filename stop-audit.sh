#!/bin/bash

sudo bash audit-rules.sh remove
sudo bash audit-rules.sh disable
echo "==== Status ====="
sudo bash audit-rules.sh status

echo "REMOVING KERNEL MODULES ===="
sudo bash rmmods.sh